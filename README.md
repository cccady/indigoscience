This is an XFWM4 theme.

You can download these files into your XFWM theme directory.
For a multi-user scenario, that is typically /usr/share/themes/IndigoScience.
For a single user scanario, that is typically ~/.themes/IndigoScience.

For more info, see the [README](xfwm4/README) in the xfwm4 directory.
