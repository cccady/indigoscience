#!/bin/bash

sed -i 's/#000000"/#000000 s active_border_color"/g' *-{active,prelight,pressed}.xpm
sed -i 's/#DAD7CA"/#DAD7CA s active_highlight_1"/g' *-{active,prelight,pressed}.xpm
sed -i 's/#BDBAAD"/#BDBAAD s active_color_1"/g' *-{active,prelight,pressed}.xpm
sed -i 's/#A59F80"/#A59F80 s active_mid_1"/g' *-{active,prelight,pressed}.xpm
sed -i 's/#807C6A"/#807C6A s active_shadow_1"/g' *-{active,prelight,pressed}.xpm
sed -i 's/#5B5746"/#5B5746 s active_shadow_1"/g' *-{active,prelight,pressed}.xpm

sed -i 's/#000000"/#000000 s inactive_border_color"/g' *-inactive.xpm
sed -i 's/#C6C6C6"/#C6C6C6 s inactive_highlight_1"/g' *-inactive.xpm
sed -i 's/#A2A2A2"/#A2A2A2 s inactive_color_1"/g' *-inactive.xpm
sed -i 's/#808080"/#808080 s inactive_mid_1"/g' *-inactive.xpm
sed -i 's/#626262"/#626262 s inactive_shadow_1"/g' *-inactive.xpm
sed -i 's/#424242"/#424242 s inactive_shadow_1"/g' *-inactive.xpm

sed -i 's/^active_text_color=/#active_text_color=/g' themerc
sed -i 's/^inactive_text_color=/#inactive_text_color=/g' themerc

xfconf-query -c xfwm4 -p /general/theme -s ''
xfconf-query -c xfwm4 -p /general/theme -s 'IndigoScience'
