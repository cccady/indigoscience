#!/bin/bash

sed -i 's/ s active_border_color"/"/g' *-{active,prelight,pressed}.xpm
sed -i 's/ s active_highlight_1"/"/g' *-{active,prelight,pressed}.xpm
sed -i 's/ s active_color_1"/"/g' *-{active,prelight,pressed}.xpm
sed -i 's/ s active_mid_1"/"/g' *-{active,prelight,pressed}.xpm
sed -i 's/ s active_shadow_1"/"/g' *-{active,prelight,pressed}.xpm

sed -i 's/ s inactive_border_color"/"/g' *-inactive.xpm
sed -i 's/ s inactive_highlight_1"/"/g' *-inactive.xpm
sed -i 's/ s inactive_color_1"/"/g' *-inactive.xpm
sed -i 's/ s inactive_mid_1"/"/g' *-inactive.xpm
sed -i 's/ s inactive_shadow_1"/"/g' *-inactive.xpm

sed -i 's/^#active_text_color=/active_text_color=/g' themerc
sed -i 's/^#inactive_text_color=/inactive_text_color=/g' themerc

xfconf-query -c xfwm4 -p /general/theme -s ''
xfconf-query -c xfwm4 -p /general/theme -s 'IndigoScience'
